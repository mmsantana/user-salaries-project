# Serviço de registro de usuários e salarys

Essa aplicação é composta por uma API que permitirá operações CRUD para modelos de usuários 
e salarys associados.

## Considerações

Cada usuário pode ter mais de um salary e as informações do usuário que a API retorna possui 
os 'endpoins' listados abaixo.

## Uso

### Modelo de usuário

É composto por cpf, nome e date de nascimento.

#### Acesso às informações

**Definição**

`GET /users`

**Response**

- `200` para sucesso

```json
[
  {
    "cpf": "12345678900",
    "name": "João da Silva",
    "birthday": "1980-08-05",
    "mean_salary": 4500.00,
    "mean_discount": 430.00,
    "biggest_salary": 7500.00,
    "smallest_salary": 3000.00
  },
  {
    "cpf": "12345678901",
    "name": "Luke Skywalker",
    "birthday": "1975-05-10",
    "mean_salary": 8000.00,
    "mean_discount": 650.00,
    "biggest_salary": 12000.00,
    "smallest_salary": 5000.00
  }
]
```

#### Registro de novo usuário

**Definição**

`POST /users`

**Argumentos**

- `"cpf":string` campo único cadastrado no sistema
- `"name":string` o nome completo do usuário
- `"birthday:string"`: date de nascimento do usuário

**Request**

```json
{
  "cpf": "12345678902",
  "name": "Walter White",
  "birthday": "1959-09-07"
}
```

**Response**

- `201` para usuário criado com sucesso
- `400` para usuário já existente

```json
{
  "cpf": "12345678902",
  "name": "Walter White",
  "birthday": "1959-09-07"
}
```

#### Atualização/substituição de usuário

**Definição**

`PUT /users/<cpf>`

**Argumentos**

- `"cpf":string` campo único cadastrado no sistema
- `"name":string` o nome completo do usuário
- `"birthday:string"`: date de nascimento do usuário

**Request**

```json
{
  "cpf": "12345678902",
  "name": "Hans Solo",
  "birthday": "1959-09-07"
}
```

**Response**

- `200` para usuário substituído com sucesso
- `204` para sem conteúdo
- `404` para usuário não encontrado

```json
{
  "cpf": "12345678902",
  "name": "Hans Solo",
  "birthday": "1959-09-07"
}
```

#### Atualização/modificação de usuário

**Definição**

`PATCH /users/<cpf>`

**Argumentos**

- `"cpf":string` campo único cadastrado no sistema
- `"name":string` o nome completo do usuário
- `"birthday:string"`: date de nascimento do usuário

**Request**

```json
{
  "cpf": "12345678903",
  "name": "Jesse Pinkman",
  "birthday": "1959-09-07"
}
```

**Response**

- `200` para usuário modificado com sucesso
- `204` para sem conteúdo
- `404` para usuário não encontrado

```json
{
  "cpf": "12345678903",
  "name": "Jesse Pinkman",
  "birthday": "1959-09-07"
}
```

#### Remoção de usuário

**Definição**

`DELETE /users/<cpf>`

**Argumentos**

- Não necessário

**Request**

- Não necessário

**Response**

- `204` para usuário removido
- `404` para usuário não encontrado



### Modelo de salary 

Armazena a date, o salary e os discount referentes a um usuário.

#### Acesso às informações

**Definição**

`GET /salaries`

**Response**

- `200` para sucesso
- `404` para salary não en
contrado

```json
[
  {
    "id": 1,
    "cpf": "12345678900",
    "date": "2019-01-10",
    "salary": 1523.35,
    "discounts": 250.00
  },
  {
    "id": 2,
    "cpf": "12345678900",
    "date": "2019-02-10",
    "salary": 1523.35,
    "discounts": 50.00
  },
  {
    "id": 3,
    "cpf": "12345678900",
    "date": "2019-03-10",
    "salary": 1600.00,
    "discounts": 0.00
  },
  {
    "id": 4,
    "cpf": "12345678901",
    "date": "2019-01-10",
    "salary": 1000.00,
    "discounts": 50.00
  },
  {
    "id": 4,
    "cpf": "12345678901",
    "date": "2019-02-10",
    "salary": 1203.00,
    "discounts": 80.00
  }
]
```

#### Registro de novo salary

**Definição**

`POST /salaries`

**Argumentos**

- `"user":string` campo referente ao usuário do sistema
- `"date":string` data referente ao salary
- `"salary:string"`: salário do usuário
- `"discounts:string"`: descontos referentes à data

**Request**

```json
{
    "id": 1,
    "user": "http://localhost:8000/users/12345678900",
    "date": "2019-10-01",
    "salary": 1523.35,
    "discounts": 250.00
}
```

**Response**

- `201` para salário criado com sucesso
- `400` para salário já existente

```json
{
    "cpf": "12345678900",
    "date": "2019-10-01",
    "salary": 2000.00,
    "discounts": 250.00
}
```

#### Atualização/substituição de salary

**Definição**

`PUT /salaries/<id>`

**Argumentos**

- `"cpf":string` campo único cadastrado no sistema
- `"date":string` data referente ao salário
- `"salary:string"`: salário do usuário
- `"discounts:string"`: descontos referentes à data

**Request**

```json
{
    "id": 1,
    "user": "http://localhost:8000/users/12345678900",
    "date": "2019-01-10",
    "salary": 1000.00,
    "discounts": 250.00
}
```

**Response**

- `200` para salary substituído com sucesso
- `204` para sem conteúdo
- `404` para salary não encontrado

```json
{
    "cpf": "12345678900",
    "date": "2019-01-10",
    "salary": 1000.00,
    "discounts": 250.00
}
```

#### Atualização/modificação de salary

**Definição**

`PATCH /salaries/<id>`

**Argumentos**

- `"cpf":string` campo único cadastrado no sistema
- `"date":string` date referente ao salary
- `"salary:string"`: salary do usuário
- `"discounts:string"`: discount referentes à date

**Request**

```json
{
    "id": 1,
    "user": "http://localhost:8000/users/12345678900",
    "date": "2019-01-10",
    "salary": 1523.35,
    "discounts": 250.00
}
```

**Response**

- `200` para salary modificado com sucesso
- `204` para sem conteúdo
- `404` para salary não encontrado

```json
{
    "cpf": "12345678900",
    "date": "2019-01-10",
    "salary": 1523.35,
    "discounts": 250.00
}
```

#### Remoção de salary

**Definição**

`DELETE /salaries/<id>`

**Argumentos**

- Não necessário

**Request**

- Não necessário

**Response**

- `204` para usuário removido
- `404` para usuário não encontrado