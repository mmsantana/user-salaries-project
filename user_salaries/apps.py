from django.apps import AppConfig


class UserSalariesConfig(AppConfig):
    name = 'user_salaries'
