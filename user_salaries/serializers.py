from rest_framework import serializers

from user_salaries.models import User, Salary


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['cpf', 'name', 'birthday']

    def to_representation(self, obj):
        return {
            "cpf": obj.cpf,
            "name": obj.name,
            "birthday": obj.birthday,
            "mean_salary": obj.mean_salary,
            "mean_discount": obj.mean_discount,
            "biggest_salary": obj.biggest_salary,
            "smallest_salary": obj.smallest_salary
        }


class SalarySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Salary
        fields = ['user', 'date', 'salary', 'discounts']

    def to_representation(self, obj):
        return {
            "id": obj.id,
            "cpf": obj.user.cpf,
            "date": obj.date,
            "salary": obj.salary,
            "discounts": obj.discounts
        }
