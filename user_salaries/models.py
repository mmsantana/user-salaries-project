from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class User(models.Model):
    cpf = models.CharField("CPF", max_length=256, unique=True, primary_key=True)
    name = models.CharField("Nome", max_length=256)
    birthday = models.DateField("Data de Nascimento")
    mean_salary = models.FloatField("Salário Médio", blank=True, null=True, default=None)
    mean_discount = models.FloatField("Disconto Médio", blank=True, null=True, default=None)
    biggest_salary = models.FloatField("Maior Salário", blank=True, null=True, default=None)
    smallest_salary = models.FloatField("Menor Salário", blank=True, null=True, default=None)

    def __str__(self):
        return f'{self.cpf} / {self.name}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.cpf = self.cpf.replace('.', '').replace('-', '')
        super(User, self).save()


class Salary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField("Data do Salário")
    salary = models.FloatField("Salário")
    discounts = models.FloatField("Descontos")

    def __str__(self):
        return f'{self.user} / {self.salary}'


@receiver(post_save, sender=Salary, dispatch_uid="update_user")
def update_user(sender, instance, **kwargs):
    salaries_models = Salary.objects.filter(user=instance.user)

    if salaries_models:
        salaries = [i.salary for i in salaries_models]
        discounts = [i.discounts for i in salaries_models]

        user = User.objects.get(cpf=instance.user.cpf)
        user.mean_salary = round(sum(salaries)/salaries_models.count(), 2)
        user.mean_discount = round(sum(discounts)/salaries_models.count(), 2)
        user.biggest_salary = max(salaries)
        user.smallest_salary = min(salaries)
        user.save()
