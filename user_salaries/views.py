from rest_framework import viewsets

from user_salaries.models import Salary, User
from user_salaries.serializers import UserSerializer, SalarySerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite a edição ou acesso dos usuários.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SalaryViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite a edição ou acesso dos salários.
    """
    queryset = Salary.objects.all()
    serializer_class = SalarySerializer
