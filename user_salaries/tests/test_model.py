import datetime
from unittest import skip

from django.test import TestCase

from user_salaries.models import User, Salary


class TestModel(TestCase):

    def create_users(self):
        User.objects.create(
            cpf='123456789000',
            name='João da Silva',
            birthday=datetime.date(1980, 8, 5)
        )
        User.objects.create(
            cpf='12345678901',
            name='Luke Skywalker',
            birthday=datetime.date(1975, 5, 10)
        )
        User.objects.create(
            cpf='123.456.789-02',
            name='Samuel Jackson',
            birthday=datetime.date(1955, 5, 10)
        )

    def create_salaries(self):
        # Salários do João da Silva
        first_user = User.objects.get(cpf='123456789000')
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 1, 10),
            salary=1523.35,
            discounts=250.0
        )
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 2, 10),
            salary=1600,
            discounts=0.0
        )
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 3, 10),
            salary=1650.35,
            discounts=150.0
        )
        # Salários do Luke Skywalker
        second_user = User.objects.get(cpf='12345678901')
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 1, 10),
            salary=1000.0,
            discounts=50.0
        )
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 2, 10),
            salary=1500.0,
            discounts=10.0
        )
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 3, 10),
            salary=3000.0,
            discounts=150.0
        )

    # @skip("Not Testing")
    def test_create_users(self):
        self.create_users()

        first_user = User.objects.get(cpf='123456789000')
        self.assertEqual(first_user.cpf, '123456789000')
        self.assertEqual(first_user.name, 'João da Silva')
        self.assertEqual(first_user.birthday, datetime.date(1980, 8, 5))

        second_user = User.objects.get(cpf='12345678901')
        self.assertEqual(second_user.cpf, '12345678901')
        self.assertEqual(second_user.name, 'Luke Skywalker')
        self.assertEqual(second_user.birthday, datetime.date(1975, 5, 10))

        third_user = User.objects.get(cpf='12345678902')
        self.assertEqual(third_user.cpf, '12345678902')
        self.assertEqual(third_user.name, 'Samuel Jackson')
        self.assertEqual(third_user.birthday, datetime.date(1955, 5, 10))

    # @skip("Not Testing")
    def test_create_salaries(self):
        self.create_users()
        self.create_salaries()
        first_user = User.objects.get(cpf='123456789000')
        first_salary = Salary.objects.filter(user=first_user)[0]
        self.assertEqual(first_salary.user.cpf, '123456789000')
        self.assertEqual(first_salary.date, datetime.date(2019, 1, 10))
        self.assertEqual(first_salary.salary, 1523.35)
        self.assertEqual(first_salary.discounts, 250.0)

        second_user = User.objects.get(cpf='12345678901')
        second_salary = Salary.objects.filter(user=second_user)[0]
        self.assertEqual(second_salary.user.cpf, '12345678901')
        self.assertEqual(second_salary.date, datetime.date(2019, 1, 10))
        self.assertEqual(second_salary.salary, 1000.0)
        self.assertEqual(second_salary.discounts, 50.0)

    # @skip("Not Testing")
    def test_create_salaries_to_set_user_values(self):
        self.create_users()

        first_user = User.objects.get(cpf='123456789000')
        self.assertEqual(first_user.cpf, '123456789000')
        self.assertEqual(first_user.mean_salary, None)
        self.assertEqual(first_user.mean_discount, None)
        self.assertEqual(first_user.biggest_salary, None)
        self.assertEqual(first_user.smallest_salary, None)

        second_user = User.objects.get(cpf='12345678901')
        self.assertEqual(second_user.cpf, '12345678901')
        self.assertEqual(second_user.mean_salary, None)
        self.assertEqual(second_user.mean_discount, None)
        self.assertEqual(second_user.biggest_salary, None)
        self.assertEqual(second_user.smallest_salary, None)

        self.create_salaries()

        first_user = User.objects.get(cpf='123456789000')
        self.assertEqual(first_user.cpf, '123456789000')
        self.assertEqual(first_user.mean_salary, 1591.23)
        self.assertEqual(first_user.mean_discount, 133.33)
        self.assertEqual(first_user.biggest_salary, 1650.35)
        self.assertEqual(first_user.smallest_salary, 1523.35)

        second_user = User.objects.get(cpf='12345678901')
        self.assertEqual(second_user.cpf, '12345678901')
        self.assertEqual(second_user.mean_salary, 1833.33)
        self.assertEqual(second_user.mean_discount, 70.0)
        self.assertEqual(second_user.biggest_salary, 3000.0)
        self.assertEqual(second_user.smallest_salary, 1000.0)


