import datetime
import json

from rest_framework.test import APITestCase, APIClient

from user_salaries.models import User, Salary


class TestUrls(APITestCase):

    def setUp(self) -> None:
        self.get_user = 'http://localhost:8000/users/123456789000/'
        self.client = APIClient()

        self.user_url = '/users/123456789000/'
        self.salary_url = '/salaries/1/'

        User.objects.create(
            cpf='123456789000',
            name='João da Silva',
            birthday=datetime.date(1980, 8, 5)
        )
        User.objects.create(
            cpf='12345678901',
            name='Luke Skywalker',
            birthday=datetime.date(1975, 5, 10)
        )

        # Salários do João da Silva
        first_user = User.objects.get(cpf='123456789000')
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 1, 10),
            salary=1523.35,
            discounts=250.0
        )
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 2, 10),
            salary=1600,
            discounts=0.0
        )
        Salary.objects.create(
            user=first_user,
            date=datetime.date(2019, 3, 10),
            salary=1650.35,
            discounts=150.0
        )

        # Salários do Luke Skywalker
        second_user = User.objects.get(cpf='12345678901')
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 1, 10),
            salary=1000.0,
            discounts=50.0
        )
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 2, 10),
            salary=1500.0,
            discounts=10.0
        )
        Salary.objects.create(
            user=second_user,
            date=datetime.date(2019, 3, 10),
            salary=3000.0,
            discounts=150.0
        )

    def test_users_get(self):
        response = self.client.get('/users/')

        self.assertEqual(response.data[0]['cpf'], '123456789000')
        self.assertEqual(response.data[0]['name'], 'João da Silva')
        self.assertEqual(response.data[0]['mean_salary'], 1591.23)

        self.assertEqual(response.data[1]['cpf'], '12345678901')
        self.assertEqual(response.data[1]['name'], 'Luke Skywalker')
        self.assertEqual(response.data[1]['mean_salary'], 1833.33)

    def test_users_post(self):
        data = {
            'cpf': '123456789002',
            'name': 'Billy Jean',
            'birthday': datetime.date(1990, 2, 13)
        }
        response = self.client.post('/users/', data)

        self.assertEqual(response.data['cpf'], '123456789002')
        self.assertEqual(response.data['name'], 'Billy Jean')
        self.assertEqual(response.data['birthday'], datetime.date(1990, 2, 13))

    def test_same_user_post(self):
        data = {
            'cpf': '123456789000',
            'name': 'Billy Jean',
            'birthday': datetime.date(1990, 2, 13)
        }
        response = self.client.post('/users/', data)

        self.assertEqual(response.data['cpf'][0], 'user with this CPF already exists.')

    def test_users_put(self):
        data = {
            'cpf': '123456789012',
            'name': 'Billy Jean',
            'birthday': datetime.date(1990, 2, 13)
        }
        response = self.client.put(self.user_url, data)

        self.assertEqual(response.data['cpf'], '123456789012')
        self.assertEqual(response.data['name'], 'Billy Jean')
        self.assertEqual(response.data['birthday'], datetime.date(1990, 2, 13))

    def test_users_patch(self):
        data = {
            'cpf': '12345678965495',
            'name': 'Hank',
            'birthday': datetime.date(1990, 2, 13)
        }
        response = self.client.patch(self.user_url, data)

        self.assertEqual(response.data['cpf'], '12345678965495')
        self.assertEqual(response.data['name'], 'Hank')
        self.assertEqual(response.data['birthday'], datetime.date(1990, 2, 13))

    def test_users_delete(self):
        response = self.client.delete(self.user_url)

        self.assertEqual(response.status_code, 204)

    def test_salaries_get(self):
        response = self.client.get('/salaries/')

        self.assertEqual(len(response.data), 6)
        self.assertEqual(response.data[0]['cpf'], '123456789000')
        self.assertEqual(response.data[3]['cpf'], '12345678901')

    def test_salaries_post(self):
        data = {
            'user': self.get_user,
            'date': datetime.date(2015, 2, 13),
            'salary': 4000.62,
            'discounts': 34.15
        }
        response = self.client.post('/salaries/', data)

        self.assertEqual(response.data['cpf'], '123456789000')
        self.assertEqual(response.data['date'], datetime.date(2015, 2, 13))
        self.assertEqual(response.data['salary'], 4000.62)
        self.assertEqual(response.data['discounts'], 34.15)

    def test_salaries_put(self):
        data = {
            'user': self.get_user,
            'date': datetime.date(1990, 2, 13),
            'salary': 6000.62,
            'discounts': 34.15
        }
        response = self.client.put(self.salary_url, data)

        self.assertEqual(response.data['cpf'], '123456789000')
        self.assertEqual(response.data['salary'], 6000.62)
        self.assertEqual(response.data['discounts'], 34.15)

    def test_salaries_patch(self):
        data = {
            'user': self.get_user,
            'date': datetime.date(1990, 2, 13),
            'salary': 6500.62,
            'discounts': 34.15
        }
        response = self.client.patch(self.salary_url, data)

        self.assertEqual(response.data['cpf'], '123456789000')
        self.assertEqual(response.data['salary'], 6500.62)
        self.assertEqual(response.data['discounts'], 34.15)

    def test_salaries_delete(self):
        response = self.client.delete(self.salary_url)

        self.assertEqual(response.status_code, 204)
